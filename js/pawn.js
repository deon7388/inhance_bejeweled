if (Modernizr.touch)
{
	var BOARD_INIT_OFFSET_X = 26;
	var BOARD_INIT_OFFSET_Y = 29;
	var PAWN_SPACING_X = 87;
	var PAWN_SPACING_Y = 86;
	var scale = 0.55;
}
else
{
	var BOARD_INIT_OFFSET_X = 25;
	var BOARD_INIT_OFFSET_Y = 32;
	var PAWN_SPACING_X = 87;
	var PAWN_SPACING_Y = 93;
	var scale = 0.58;
}



function setPawn(x, y, value)
{	
	//var path = './assets/sprites/' + value + '.png';
	var path = './assets/sprites/' + value + '/0.png';
	var texture = PIXI.Texture.fromImage(path);
	var pawn = new PIXI.Sprite(texture);
	
	pawn.id = 'block' + y + '_' + x;
	pawn.spritePath = path;
	pawn.scale.x = scale;
	pawn.scale.y = scale;

	//position
	pawn.x = BOARD_INIT_OFFSET_X + (PAWN_SPACING_X * x);
	pawn.y = BOARD_INIT_OFFSET_Y + (PAWN_SPACING_Y * y);

	pawn.xCoor = pawn.x;
	pawn.yCoor = pawn.y;

	pawn.xGridPos = x; //for simpler transitioning using units of 1
	pawn.yGridPos = y;

	pawn.isSelected = false;
	pawn.isSwapping = false;
	pawn.isRemoving = false;

	//clicking the sprite
	pawn.setInteractive(true);
	pawn.click = pawn.tap = function(data)
	{	
		//if (this.isSwapping || pawn.isRemoving) { return; }
		if (wholeAnimationInProgress) { return; }

		var newTexture = PIXI.Texture.fromImage(this.spritePath.replace('.png', '_dn.png'));

		//clear selected if not adjacent to the target
		for (var i = 0; i < stage.children.length; i++) 
		{
			var child = stage.children[i];
			if (!isAdjacentTo(child, this) && child.texture.baseTexture.imageUrl.indexOf('_dn') != -1) //if in clicked state and not adjacent to the last clicked
			{
				var oldTexture = PIXI.Texture.fromImage(child.texture.baseTexture.imageUrl.replace('_dn.png', '.png'));
				child.setTexture(oldTexture);
				child.isSelected = false;
			}
			else if (child == this && this.texture.baseTexture.imageUrl.indexOf('_dn') != -1)
			{
				var oldTexture = PIXI.Texture.fromImage(this.texture.baseTexture.imageUrl.replace('_dn.png', '.png'));
				this.setTexture(oldTexture);
				this.isSelected = false;
				clearArray(pawnsToSwap);
				return; //no need to finish if the same is clicked
			}
		}

		this.setTexture(newTexture);
		this.isSelected = true;
		pawnsToSwap.push(this);

		if (pawnsToSwap.length == 2)
		{
			swapPawns(pawnsToSwap[0], pawnsToSwap[1], false);
			clearArray(pawnsToSwap); //fastest way to empty
		}
	}

	return pawn;
};


var isAdjacentTo = function(source,target)
{
	return ((source.xGridPos === target.xGridPos - 1 || source.xGridPos === target.xGridPos + 1 || source.xGridPos === target.xGridPos) && source.yGridPos === target.yGridPos)
			 || ((source.yGridPos === target.yGridPos - 1 || source.yGridPos === target.yGridPos + 1 || source.yGridPos === target.yGridPos) && source.xGridPos === target.xGridPos);
};

var checkForAdjacentPawn = function(source, horizontal, direction) //boolean, -1/1 to define left/right or down/up, respectively
{
	var adjacentPawn;
	var group = [];
	var x = source.xGridPos;
	var y = source.yGridPos;
	
	var i = (horizontal ? x : y) + direction; //start with the next pawn (either left/right/up/down depending on flag)

	//Go as far as we can in one direction searching for alike kinds adjacent to starting pawn.
	//Basically if we do not find at least 2 directly adjacent, then it's a bust but we'll go all the way because
	//the grid is not large - if we do 100x100 grid or bigger one day, make source code more efficient 
	for (; (direction == -1) ? (i > -1) : (i < GRID_SIZE); i += direction)  
	{
		adjacentPawn = horizontal ? findWithProp(stage.children, 'id', 'block' + y + '_' + i) : findWithProp(stage.children, 'id', 'block' + i + '_' + x);
		
		//same sprite picture
		if (adjacentPawn && adjacentPawn.spritePath == source.spritePath) 
		{
			group = group.concat(adjacentPawn);
		}
		else 
		{
			break;
		}
	}

	return group;
};

var checkRowsForThree = function(source)
{
	var hGroup = [];
	hGroup.push(source);

	//check left side
	if (source.xGridPos > 0)
	{
		hGroup = hGroup.concat(checkForAdjacentPawn(source, true, -1));
	}

	//check right side
	if (source.xGridPos < GRID_SIZE - 1)
	{
		hGroup = hGroup.concat(checkForAdjacentPawn(source, true, 1)); 
	}

	return hGroup;
};

var checkColumnsForThree = function(source)
{
	var vGroup = [];
	vGroup.push(source);

	//check above
	if (source.yGridPos > 0)
	{
		vGroup = vGroup.concat(checkForAdjacentPawn(source, false, -1));
	}

	//check below
	if (source.yGridPos < GRID_SIZE - 1)
	{
		vGroup = vGroup.concat(checkForAdjacentPawn(source, false, 1)); 
	}

	return vGroup;
};

var anyMovesHorizontal = function(source, direction)
{
	var adjacentPawn;
	var x = source.xGridPos;
	var y = source.yGridPos;
	
	var i = x + direction; //start with the next pawn

	adjacentPawn = findWithProp(stage.children, 'id', 'block' + y + '_' + i);
		
	//if we have two alike, then look at a possible 3rd pawn for match
	if (adjacentPawn && adjacentPawn.spritePath == source.spritePath) 
	{
		i += direction; 
		for (var move = 1; move > -2; move--) 
		{
			//try the one below, next to and above 
			adjacentPawn = move == 0 ? findWithProp(stage.children, 'id', 'block' + y + '_' + (i + direction)) : findWithProp(stage.children, 'id', 'block' + (y + move) + '_' + i);
			if (adjacentPawn && adjacentPawn.spritePath == source.spritePath) 
			{
				return true;
			}
		}
	}
	else if (adjacentPawn) //see if there is a pawn that can slide in the middle of 2 alike
	{
		for (var move = 1; move > -2; move--) 
		{
			//try the one below and above the non-match
			if (move != 0) 
			{ 
				adjacentPawn = findWithProp(stage.children, 'id', 'block' + (y + move) + '_' + i); 
				if (adjacentPawn && adjacentPawn.spritePath == source.spritePath) 
				{
					i += direction; //now go to the 3rd and make sure it's there
					adjacentPawn = findWithProp(stage.children, 'id', 'block' + y + '_' + i); //should be one on the same y pos
					if (adjacentPawn && adjacentPawn.spritePath == source.spritePath) 
					{
						return true;
					}
					else
					{
						break;
					}
				}
				else
				{
					continue;
				}
			}
		}
	}

	return false;
};

var anyMovesVertical = function(source, direction)
{
	var adjacentPawn;
	var x = source.xGridPos;
	var y = source.yGridPos;
	
	var i = y + direction; //start with the next pawn

	adjacentPawn = findWithProp(stage.children, 'id', 'block' + i + '_' + x);
		
	//if we have two alike, then look at a possible 3rd pawn for match
	if (adjacentPawn && adjacentPawn.spritePath == source.spritePath) 
	{
		i += direction; 
		for (var move = 1; move > -2; move--) 
		{
			//try the one left, above and right 
			adjacentPawn = move == 0 ? findWithProp(stage.children, 'id', 'block' + (i + direction) + '_' + x) : findWithProp(stage.children, 'id', 'block' + i + '_' + (x + move));
			if (adjacentPawn && adjacentPawn.spritePath == source.spritePath) 
			{
				return true;
			}
		}
	}
	else if (adjacentPawn) //see if there is a pawn that can slide in the middle of 2 alike
	{
		for (var move = 1; move > -2; move--) 
		{
			//try the one below and above the non-match
			if (move != 0)
			{
				adjacentPawn = findWithProp(stage.children, 'id', 'block' + i + '_' + (x + move));
				if (adjacentPawn && adjacentPawn.spritePath == source.spritePath) 
				{
					i += direction; //now go to the 3rd and make sure it's there
					adjacentPawn = findWithProp(stage.children, 'id', 'block' + i + '_' + x); //should be one on the same y pos
					if (adjacentPawn && adjacentPawn.spritePath == source.spritePath) 
					{
						return true;
					}
					else
					{
						break;
					}
				}
				else
				{
					continue;
				}
			}
			
		}
	}

	return false;
};