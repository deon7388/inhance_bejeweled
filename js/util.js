function getRandom(min, max) 
{
	//making sure we only get whole numbers
	var floor = Math.random > 0.5; 
  	return floor ? Math.floor(Math.random() * (max - min) + min) : Math.ceil(Math.random() * (max - min) + min);
}

function clearArray(array)
{
	while (array.length > 0)
	{
		array.pop();
	}
}

function containsObject(obj, list) 
{
    for (var i = 0; i < list.length; i++) 
    {
        if (list[i] === obj) 
        {
            return true;
        }
    }

    return false;
}

function findWithProp(array, attr, value) 
{
    for(var i = 0; i < array.length; i += 1) 
    {
        if(array[i][attr] === value) 
        {
            return array[i];
        }
    }
}

function switchPawnProps(pawn1, pawn2)
{
    var tempPawn1 = $.extend(true, {}, pawn1);
    var tempPawn2 = $.extend(true, {}, pawn2);

    for (var key in tempPawn1)
    {
        if (key != 'texture' && key != 'spritePath') { pawn2[key] = tempPawn1[key]; } //don't change the pictures' data because it's already going to move
    }

    for (var key in tempPawn2)
    {
        if (key != 'texture' && key != 'spritePath') { pawn1[key] = tempPawn2[key]; }
    }

    pawn1.x = pawn1.xCoor;
    pawn1.y = pawn1.yCoor;

    pawn2.x = pawn2.xCoor;
    pawn2.y = pawn2.yCoor;

    //switch the indices of the two pawns
    var pawn1Index = stage.children.indexOf(pawn1);
    var pawn2Index = stage.children.indexOf(pawn2);
    var tmpPawn = stage.children[pawn1Index];
    stage.children[pawn1Index] = stage.children[pawn2Index];
    stage.children[pawn2Index] = tmpPawn;
}


function dynamicSort(property) { 
    return function (obj1,obj2) {
    	if (obj1 != null && obj2 != null)
    	{
    		return obj1[property] > obj2[property] ? 1
            : obj1[property] < obj2[property] ? -1 : 0;
    	}
        
    }
}

function dynamicSortMultiple() {
    /*
     * save the arguments object as it will be overwritten
     * note that arguments object is an array-like object
     * consisting of the names of the properties to sort by
     */
    var props = arguments;
    return function (obj1, obj2) {
        var i = 0, result = 0, numberOfProperties = props.length;
        /* try getting a different result from 0 (equal)
         * as long as we have extra properties to compare
         */
        while(result === 0 && i < numberOfProperties) {
            result = dynamicSort(props[i])(obj1, obj2);
            i++;
        }
        return result;
    }
}

function anyElementUndefined(element, index, array)
{
	return (element === undefined);
}

function resetPawnIds()
{
    var x = 0;
    var y;
    for (var h = 0; h < stage.children.length; h++) 
    {
        y = Math.floor(h/GRID_SIZE);
        stage.children[h].id = 'block' + y + '_' + x;

        if (stage.children[h].spritePath != stage.children[h].texture.baseTexture.imageUrl) //if the image path hasn't changed
        {
            stage.children[h].texture.baseTexture.imageUrl = stage.children[h].spritePath;
        }

        if (x == GRID_SIZE - 1) {x = 0;}
        else
        {
            x++;
        }
    }
}

function resetPawnPosition()
{
    for (var h = 0; h < stage.children.length; h++) 
    {
        if (stage.children[h].y != stage.children[h].yCoor)
        {
            stage.children[h].y = stage.children[h].yCoor;
        }

        if (stage.children[h].x != stage.children[h].xCoor)
        {
            stage.children[h].x = stage.children[h].xCoor;
        }
    }
}


function pawnsInOrder()
{
    var x = 0;
    var y = 0;
    var inOrder = true;
    var id;
    var index;

    for (var h = 0; h < stage.children.length; h++) 
    {
        y = Math.floor(h/GRID_SIZE);
        if (stage.children[h].id == 'block' + y + '_' + x)
        {
            if (x == GRID_SIZE - 1) {x = 0;}
            else
            {
                x++;
            }
        }
        else
        {
            id = stage.children[h].id;
            index = h;
            inOrder = false;
            break;
        }
    }

    return inOrder ? "All sprites in order" : { inOrder: inOrder, id: id, index: index };
}

function resetStrayPawn()
{
    var strayPawn = pawns[-1];
    if (strayPawn)
    {
        for (var h = 0; h < pawns.length; h++) 
        {
            if(pawns[h] === undefined)
            {
                pawns[h] = strayPawn;
            }
        }
    }
}

function findAnyMisMatchSprites()
{
    var array = [];
    for (var p = 0; p < pawns.length; p++)
    {
        if (pawns[p] != null)
        {
            var pawnSpritePath = pawns[p].spritePath;
            var stageSpritePath = stage.children[p].texture.baseTexture.imageUrl;
            
            if (pawnSpritePath != stageSpritePath)
            {
                array.push({
                    pawn: pawns[p],
                    index: p,
                    dataPawnSprite: pawns[p].spritePath, 
                    stageSprite: stage.children[p].texture.baseTexture.imageUrl
                });
            }
        }
        
    }

    if (array.length) { console.log(array); return; }

    console.log('All sprites match');
    return;
}

function findIsVerticalGroup(group)
{
    return group[2].xGridPos != group[1].xGridPos;
}

function reconcileCommonPawns(group)
{
    var groups = [];
    var innerGroup1 = group[0];
    var innerGroup2 = group[1];
    for (var k = 0; k < innerGroup1.length; k++) 
    {
        for (var g = 0; g < innerGroup2.length; g++)
        {
            if (innerGroup1[k] == innerGroup2[g])
            {
                innerGroup2[g] = null;
                groups.push(innerGroup1, innerGroup2);
                return groups;
            }
        }
    }
}

function getURLParameter(sParam)
{
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++)
    {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam)
        {
            return sParameterName[1];
        }
    }
}


function rigGame(value, index)
{
    var path = './assets/sprites/' + value + '/0.png';
    var texture = PIXI.Texture.fromImage(path);

    stage.children[index].setTexture(texture);
    stage.children[index].spritePath = path;
}