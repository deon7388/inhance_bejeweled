/*===============================================================================================*/
/*=====================================HELPER FUNCTIONS==========================================*/
/*===============================================================================================*/

function automaticallyRemoveGroups(beforeGame)
{
	//do not allow automatic removal if the user is removing
	if (!isManuallyRemovingGroups && !wholeAnimationInProgress)
	{
		(function lookForGroups(v)
		{
			if (stage.children[v])
			{
				var group;
				if (stage.children[v].texture.baseTexture.imageUrl != EMPTY_TEXTURE_PATH)
				{
					group = checkForGroups(stage.children[v]);
					if (group.length > 0)
					{
						removeGroups(group, [], beforeGame);
						lookForGroups(++v);
					}
					else
					{
						lookForGroups(++v);
					}
				}
				else
				{
					return;
				}
			}else
			{
				return;
			}
			
		})(0);
	}
}

function slideAndAdd(pawnsEliminated, isVerticalGroup, beforeGame, callback)
{
	var audioPlayed = false;

	//move the pawns above down the number of spots that were removed
	(function iterateEliminated(g)
	{
		if (pawnsEliminated[g])
		{
			(function iterateChildren(p)
			{
				if (stage.children[p])
				{
					var child = stage.children[p];
					var isAboveRemoved = isVerticalGroup ? 
											child.yGridPos < pawnsEliminated[g].yGridPos && child.xGridPos == pawnsEliminated[0].xGridPos :
											child.yGridPos < pawnsEliminated[g].yGridPos && child.xGridPos == pawnsEliminated[g].xGridPos; 
											
					
					//making sure we're not finding the actual removed piece but what's above them if vertical
					isAboveRemoved = isVerticalGroup ? isAboveRemoved && pawnsEliminated.indexOf(child) == -1 : isAboveRemoved; 

					if (isAboveRemoved && child != pawnsEliminated[g])
					{
						if (!audioPlayed && !beforeGame) { bangSound.play(); audioPlayed = true; }

						var index = stage.children.indexOf(pawnsEliminated[g]);
						TweenMax.to(child, 0.1, {y: stage.children[index].yCoor, onStart: function()
						{
							switchPawnProps(child, stage.children[index], true);
							iterateChildren(--p);
						}});
						
					}
					else
					{
						iterateChildren(--p);
					}
				}
				else
				{
					iterateEliminated(++g);
				}
				
			})(stage.children.length - 1);
		}
		else
		{
			// removeAnimationInProgress = false;
			if (callback && typeof callback === 'function' && !beforeGame)
			{
				callback();
			}
		}
	})(0);

	
	//add new pawns in the blank spots
	if (beforeGame)
	{
		setTimeout (function()
		{
			addPawns();
		}, 300);
	}
}


function addPawns()
{
	for (var c = 0; c < stage.children.length; c++) 
	{
		var child = stage.children[c];

		var texturePath = stage.children[c].texture.baseTexture.imageUrl;
		if (texturePath == EMPTY_TEXTURE_PATH)
		{
			var x = child.xGridPos;
			var y = child.yGridPos;
			var newChild = setPawn(x, y, getRandom(0,NUM_OF_PAWN_ICONS));

			//give the child the new child's props that it doesn't have
		    child.setTexture(newChild['texture']);
		    child['spritePath'] = newChild['spritePath'];

		    isManuallyRemovingGroups = false;
		    wholeAnimationInProgress = false;
		}
	}
}

function createCountDown(timeRemaining) 
{
    var startTime = Date.now();
    return function() {
       return Math.ceil( (timeRemaining - Math.abs( Date.now() - startTime )) / 1000 );
    }
}

function calculateScore(pawnsEliminated)
{
	var eliminatedScore = 0;
	switch (pawnsEliminated.length)
	{
		case 3:
			eliminatedScore = 3048;
			break;

		case 4:
			eliminatedScore = 4218;
			break;

		case 5:
			eliminatedScore = 6540; //most possible for one group
			break;

		default:
			eliminatedScore = 0;
			break;
	}
	//console.log(pawnsEliminated.length + ' - ' + eliminatedScore);

	GAME_SCORE += eliminatedScore;
	$('#score > div').text(GAME_SCORE);
}

function getUser()
{
	var name = getURLParameter('name');
	name = name ? name.replace(/%20/g, ' ') : 'Test'; 
	GAME_PLAY_TIME = getURLParameter('t') != undefined ? getURLParameter('t') : 60000;
	session_id = getURLParameter('session_id');
	console.log(name + ' ' + GAME_PLAY_TIME + ' ' + session_id); 

	$('#name > div').text(name);
}

function sendUserScore()
{
	setTimeout(function()
	{
		$('#gridFrame').css('opacity', 0);
		$('#grid').css('opacity', 0);
		$('canvas').css('opacity', 0);
		$('#gameInfo').css('opacity', 0);
		$('#timerBlock').css('opacity', 0);

		location.href = '/trivia/bonusreturn?score=' + GAME_SCORE.toString() + '&session_id=' + session_id;
	}, 3000);
}

function checkTimeLeft()
{
	countDownValue = currentCountDown();
	if (countDownValue <= 0 && !wholeAnimationInProgress)
	{
		$('#timerBlock span').text(0);
		clearInterval(timer);
		endGame();
	}
	else if (countDownValue <= 0 && wholeAnimationInProgress) //if the last move has been made before the timer runs out, allow it to finish before saying game over
	{
		$('#timerBlock span').text(0);
		var endGameInterval = setInterval(function()
		{
			if (!wholeAnimationInProgress)
			{
				clearInterval(endGameInterval);
				endGame();
			}
		}, 1);
	}
	else if (countDownValue <= 5 && countDownValue > 0)
	{
		timerSound.play();
		$('#timerBlock span').text(countDownValue);
	}
	else
	{
		$('#timerBlock span').text(countDownValue);
	}
}

function checkMovesLeft()
{
	var allMovesCount = 0;
	for (var i = 0; i < stage.children.length; i++) 
	{
		var hasMovesLeft = anyMovesHorizontal(stage.children[i], -1);
		var hasMovesRight = anyMovesHorizontal(stage.children[i], 1);

		var hasMovesAbove = anyMovesVertical(stage.children[i], -1);
		var hasMovesBelow = anyMovesVertical(stage.children[i], 1);

		if (hasMovesLeft) { allMovesCount++; }
		if (hasMovesRight) { allMovesCount++; }
		if (hasMovesAbove) { allMovesCount++; }
		if (hasMovesBelow) { allMovesCount++; }
	}

	if (allMovesCount == 0 && (countDownValue > 0 || isNaN(countDownValue))) 
	{
		refreshGame();
	}
}

function endGame()
{
	$('#gameOver p').text("GAME OVER");

	if (Modernizr.touch) { 
		$('#gameOver p').css({ left: '20%', top: '25%', fontSize: '6em'}); 
	}
	else
	{
		$('#gameOver p').css({ left: '35%', top: '30%', fontSize: '6em'}); 
	}

	$('#gameOver').fadeIn("fast");
	shrilloSound.play();	
	sendUserScore();
}

function refreshGame()
{
	$('#gameOver p').text("Sorry, no moves are left. We'll refresh this for you.");
	$('#gameOver p').css({ fontSize: '27pt'});
	if (Modernizr.touch) { $('#gameOver p').css({ left: '10%'}); }
	$('#gameOver').fadeIn("fast");
	
	createBoard(true); //refresh the board instead of the page
	setTimeout(function() { $('#gameOver').fadeOut("fast"); }, 2000);
}