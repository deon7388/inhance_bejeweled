var renderer, stage, width, height;
var pawnsToSwap = [];
var GRID_SIZE = 8;
var NUM_OF_PAWN_ICONS = 8;
var MAX_PAWNS = Math.pow(GRID_SIZE, 2);
var EMPTY_TEXTURE_PATH = './assets/sprites/empty.png';
var GAME_PLAY_TIME;// = 30000* 20;
var GAME_SCORE = 0;
var sesssion_id;
var isManuallyRemovingGroups = false;
var wholeAnimationInProgress = false;
var removeAnimationInProgress = false;
var groupingBeforeStart = true;
var userInfo;
var canShowBoard = false;
var startTimer = false;
var currentCountDown;
var countDownValue;
var timer;
var noMovesInterval;


/*==========sound effects=========*/
var buzzSound = new Audio('./assets/sounds/bottle_buzz.mp3');
var timerSound = new Audio('./assets/sounds/countdown.mp3');
var bangSound = new Audio('./assets/sounds/metallic_bang.mp3');
var shrilloSound = new Audio('./assets/sounds/shrillo.mp3');
var swishSound = new Audio('./assets/sounds/swish.mp3');
/*================================*/


/*=======IF GAME ON COMPUTER, CHANGE DIMENSIONS=====*/
if (Modernizr.touch)
{
	width = $(window).width() * 0.75;
	height = $(window).height();
}
else
{
	width = $(window).width() * 0.65;
	height = $(window).height() * 0.75;
}
/*=================================================*/

$(document).ready(function()
{
	getUser();
	startGame();
});


function startGame()
{
	createBoard(false);
	$.merge($('body > div'), $('canvas')).not('#instructions').hide();
	$('#skipButton').css('opacity', 0);

	setTimeout(function() { $('#skipButton').animate({opacity: 1}, 700); }, 5000);

	fadeInstructions();

	$('#skipButton').click(function(e)
	{
		if ($(this).css('opacity') == 1)
			canShowBoard = true;
	});

	var showBoardInterval = setInterval(function()
	{
		if (canShowBoard)
		{
			clearInterval(showBoardInterval);
			groupingBeforeStart = false;
			$.merge($('body div'), $('canvas')).show();
			$.merge($('#instructions'), $('#gameOver')).hide();


			//show a countdown before the game starts
			var cnt = 3;
			$('#countToStart').delay(1000).text(3).fadeIn(function()
			{
				var countInterval = setInterval(function()
				{
					cnt--;
					if (cnt == 0)
					{
						clearInterval(countInterval);
						$('#countToStart').css({'margin-left': '-5%'}).text('GO!');
						$('#countToStart').fadeOut(700);
						setTimeout(function() { $('#countContainer').css({zIndex: -50}); }, 50);
						startTimer = true;
					}
					else
					{
						$('#countToStart').text(cnt);
					}
				}, 1000);

				//check the amount of time left - but wait until we can start the timer first
		    	var timerInterval = setInterval(function()
		    	{
		    		if (startTimer)
		    		{
		    			clearInterval(timerInterval);
		    			//creating a coundown timer
				        currentCountDown = createCountDown(GAME_PLAY_TIME);

						timer = setInterval(function()
						{
							checkTimeLeft();
						}, 50);
		    		}
		    	}, 50);


			    //checking for moves left in the game
			    //if none, game board will refresh and the timer will keep going
			    noMovesInterval = setInterval(function()
				{
					if (!wholeAnimationInProgress) { checkMovesLeft(); }
				}, 5);

				//removing any groups automatically
				setInterval(function()
				{
					automaticallyRemoveGroups(false);
				}, 300);

				setInterval(function()
				{
					if (!wholeAnimationInProgress && !isManuallyRemovingGroups) { addPawns(); }
				}, 500);
			});
		}
	}, 100);

	
	function fadeInstructions()
	{
		$('.instructions').css('opacity', 0);
		$('#welcome').delay(1000).animate({opacity: 1}, 500);

		var removeGroupsInt = setInterval(function()
		{
			automaticallyRemoveGroups(true);
		}, 300);

		var instNum = 0;
		var pathInterval;
		var instructionsInterval = setInterval(function()
		{
			instNum++;
			
			if (instNum > 5)
			{
				clearInterval(instructionsInterval);
				clearInterval(removeGroupsInt);

				$('#goodLuck').animate({opacity: 1}, 500);
				setTimeout(function() 
				{ 
					if (!canShowBoard) { canShowBoard = true; }
					clearInterval(pathInterval); 
				}, 3000);
			}
			else if (instNum == 3)
			{
				$('#instruction' + instNum).animate({opacity: 1}, 500);
				
				var imgState = 1;
				var startPath = './assets/instructions/';
			  	var nextImage = startPath + imgState + '.png';

			  	//show image sequence that demonstrates pieces swapping
			  	pathInterval = setInterval(function()
			  	{
			  		$('#instruction3 > div').css('background-image', 'url(./assets/instructions/'+ imgState +'.png)');

			  		if (imgState < 4) { imgState++; }
			  		else
			  		{
			  			imgState = 1;
			  		}
			  	}, 400);
			}
			else
			{
				$('#instruction' + instNum).animate({opacity: 1}, 500);
			}
		}, 4500);
	}
}


function createBoard(restart)
{
	if (!restart) //beginning of game
	{
		renderer = new PIXI.CanvasRenderer(width, height, null, true);
	    $(document.body).append(renderer.view);

		stage = new PIXI.Stage;	

		var index = 0;
	    for(var i = 0, j = 0; i < GRID_SIZE; i++) //re-declare j each time to start a new row, i is the column
	    {
	    	for (j = 0; j < GRID_SIZE; j++) 
	    	{
	    		var pawn = setPawn(j, i, getRandom(0,NUM_OF_PAWN_ICONS)); //value will change to numbers later
	    		stage.addChild(pawn);
			}
	    }

    	requestAnimationFrame(animate);
	}
	else
	{
		//just refresh the pieces
		for (var i = 0; i < stage.children.length; i++) {
			var value = getRandom(0, NUM_OF_PAWN_ICONS);
			var path = './assets/sprites/' + value + '/0.png';
			var texture = PIXI.Texture.fromImage(path);
			stage.children[i].spritePath = path;
			stage.children[i].setTexture(texture);
			stage.children[i].isSelected = false;
			stage.children[i].isSwapping = false;
			stage.children[i].isRemoving = false;
		}		

		isManuallyRemovingGroups = false;
		wholeAnimationInProgress = false;
		clearArray(pawnsToSwap);
	}
}


function animate()
{
	renderer.render(stage);
	requestAnimationFrame(animate);
}


function swapPawns(pawn1, pawn2, isSwapBack)
{
	var duration = 0.5;

	if (isAdjacentTo(pawn1, pawn2))
	{
		if(pawn1.xCoor != pawn2.xCoor)
		{
			pawn1.isSwapping = true;
			pawn2.isSwapping = true;
			
			TweenMax.to(pawn1, duration, {x: pawn2.xCoor});
			TweenMax.to(pawn2, duration, {x: pawn1.xCoor, onComplete: function()
			{
				swapDataAndRemove();
			}});
		}
		else if (pawn1.yCoor != pawn2.yCoor)
		{
			pawn1.isSwapping = true;
			pawn2.isSwapping = true;

			TweenMax.to(pawn1, duration, {y: pawn2.yCoor});
			TweenMax.to(pawn2, duration, {y: pawn1.yCoor, onComplete: function()
			{
				swapDataAndRemove();
			}});
		}

		pawn1.setTexture(PIXI.Texture.fromImage(pawn1.spritePath.replace('_dn.png', '.png')));
		pawn2.setTexture(PIXI.Texture.fromImage(pawn2.spritePath.replace('_dn.png', '.png')));

		pawn1.isSelected = false;
		pawn2.isSelected = false;
	}
	else
	{
		pawn1.setTexture(PIXI.Texture.fromImage(pawn1.spritePath.replace('_dn.png', '.png')));
		pawn2.setTexture(PIXI.Texture.fromImage(pawn2.spritePath.replace('_dn.png', '.png')));

		pawn1.isSelected = false;
		pawn2.isSelected = false;
	}

	function swapDataAndRemove()
	{
		switchPawnProps(pawn1, pawn2, false);

		if (isSwapBack) //stop at this point if we are swapping back after not finding any groups of 3 (no infinite loops)
		{ 
			clearArray(pawnsToSwap); 
			pawn1.isSwapping = false;
			pawn2.isSwapping = false;
			return; 
		} 

		//must check groups for both pawns swapped
		var pawn1Group = checkForGroups(pawn1);
		var pawn2Group = checkForGroups(pawn2);


		//check if the 2 groups have pawns in common
		//if so, make that pawn null in one array to avoid conflict with removal
		if (pawn1Group.length > 1) { pawn1Group = reconcileCommonPawns(pawn1Group); }
		else if (pawn2Group.length > 1) { pawn2Group = reconcileCommonPawns(pawn2Group); }


		if (pawn1Group.length == 0 && pawn2Group.length == 0) 
		{ 
			swapPawns(pawn1, pawn2, true, false); 
			buzzSound.play(); 
		}
		else
		{
			//remove the group(s)
			isManuallyRemovingGroups = true;
			removeGroups(pawn1Group, pawn2Group);
			pawn1.isSwapping = false;
			pawn2.isSwapping = false;
		}
	}
}


function checkForGroups(pawn)
{
	var groups = [];
	var hGroup = checkRowsForThree(pawn);
	var vGroup = checkColumnsForThree(pawn);

	if (hGroup.length > 2)
	{
		groups.push(hGroup);
	}

	if (vGroup.length > 2)
	{
		groups.push(vGroup);
	}

	return groups;
}


function removeGroups(pawn1Group, pawn2Group, beforeGame)
{
	if (wholeAnimationInProgress || removeAnimationInProgress) { return; } 
	wholeAnimationInProgress = true;
	removeAnimationInProgress = true;

	
	var allGroups = [];
	allGroups = pawn2Group ? allGroups.concat(pawn1Group, pawn2Group) : pawn1Group;

	var topProps = [];

	if (allGroups.length == 1)
	{
		var stats1 = iterateGroups(0);

		//add new pawns in the blank spots
		var addInterval = setInterval(function()
		{
			if (!removeAnimationInProgress)
			{
				clearInterval(addInterval);
				slideAndAdd(stats1.eliminated, stats1.isVerticalGroup, beforeGame); 
				setTimeout(function(){ addPawns(); }, 300);
				setTimeout(function(){ resetPawnPosition(); }, 500);  //if a pawn accidentally slides to the wrong position
				//addPawns();
			}
		}, 400);
	}
	else if (allGroups.length == 2) //make sure it both groups remove at the same time
	{	
		var stats1 = iterateGroups(0);
		var stats2 = iterateGroups(1);

		//add new pawns in the blank spots
		var addInterval = setInterval(function()
		{
			if (!removeAnimationInProgress)
			{
				clearInterval(addInterval);
				slideAndAdd(stats1.eliminated, stats1.isVerticalGroup, beforeGame, function()
				{
					slideAndAdd(stats2.eliminated, stats2.isVerticalGroup, beforeGame);
					setTimeout(function(){ addPawns(); }, 400);
					setTimeout(function(){ resetPawnPosition(); }, 500);  //if a pawn accidentally slides to the wrong position
				}); 
			}
		}, 400);
	}
	else if (allGroups.length == 0)
	{
		wholeAnimationInProgress = false;
		removeAnimationInProgress = false;
	}


	function iterateGroups(i)
	{
		var pawnsEliminated = [];
		var isVerticalGroup;
		if (allGroups[i])
		{
			isVerticalGroup = true;
			var innerGroup = allGroups[i]; 
			innerGroup.sort(dynamicSortMultiple('yGridPos', 'xGridPos'));
			if (innerGroup[2].xGridPos != innerGroup[1].xGridPos) { isVerticalGroup = false; } //is this a horizontal or vertical group?
			var imgState = 0;
			if (!beforeGame) { swishSound.play(); }
			(function doRemoveAnim(k)
			{
				if (innerGroup[k] && innerGroup[k] != null)
				{
					var pawnToRemove = innerGroup[k];
					pawnToRemove.isRemoving = true;

					//instead of removing the actual sprite (too hard to keep up with),
					//fake removing it by placing an empty png in its place at the end of the png sequence
					
					if (imgState < 20)
					{
						var startPath = pawnToRemove.texture.baseTexture.imageUrl.substring(0, pawnToRemove.texture.baseTexture.imageUrl.lastIndexOf('/') + 1);
					  	var nextImage = startPath + imgState + '.png';
					  	var nextTexture = PIXI.Texture.fromImage(nextImage);
						pawnToRemove.setTexture(nextTexture);
						imgState++;
						if (beforeGame)
						{
							doRemoveAnim(k);
						}
						else
						{
							setTimeout(function() { doRemoveAnim(k);  }, 1);
						} 
					}
					else if (imgState == 20)
					{
						var emptyTexture = PIXI.Texture.fromImage(EMPTY_TEXTURE_PATH);
						pawnToRemove.setTexture(emptyTexture);
						pawnsEliminated.push(pawnToRemove);							
						imgState = 0;
						
						doRemoveAnim(--k);
					}
				}
				else
				{
					setTimeout( function() 
					{ 
						for (var h = 0; h < pawnsEliminated.length; h++)
						{
							pawnsEliminated[h].isRemoving = false;
							pawnsEliminated[h].isSwapping = false;
						}

						removeAnimationInProgress = false;

					}, 50);

					//keep score
					if (!groupingBeforeStart) { calculateScore(pawnsEliminated); }
					//iterateGroups(++i);
				}
			})(innerGroup.length - 1);

			return {
				eliminated: pawnsEliminated,
				isVerticalGroup: isVerticalGroup
			};
		}
	}
}
